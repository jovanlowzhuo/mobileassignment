package com.example.jovanlow.batterygame_mgp2018lab1;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Splashpage extends Activity implements OnTouchListener{

    private TextView startsplash;
    private ImageView loading;
    int _splashTime = 1000;
    boolean _active = true;
    float startScaleX, startScaleY;

    private RelativeLayout layout;
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE); //hide title, MUST BE PUT BEFORE SETCONTENTVIEW
        setContentView(R.layout.splashpage);

        //requestWindowFeature(Window.FEATURE_NO_TITLE); //hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //hide top bar

        loading = (ImageView) findViewById(R.id.loading);
        loading.setScaleX(0);
        startsplash = (TextView) findViewById(R.id.startsplash);
        startScaleX = startsplash.getScaleX();
        startScaleY = startsplash.getScaleY();

        layout = (RelativeLayout)findViewById(R.id.myRelativeLayout);
        layout.setOnTouchListener(this);

    }

    //@Override
    //public boolean onTouchEvent(MotionEvent event){
        //if(event.getAction() == MotionEvent.ACTION_DOWN){
          //  _active = false;
        //}
      //  return true;
    //}

    @Override
    public boolean onTouch(View v, MotionEvent m) {
        if (m.getAction()== MotionEvent.ACTION_DOWN) {
            //_active = false;
            startsplash.setScaleX((float)(startScaleX * 0.9));
            startsplash.setScaleY((float)(startScaleY * 0.9));
            startsplash.setTextColor(Color.parseColor("#DDDDDD"));
            return true;
        }
        else if (m.getAction() == MotionEvent.ACTION_UP) {
            //thread for displaying the Splash Screen
            startsplash.setScaleX(0);
            startsplash.setScaleY(0);
            ObjectAnimator animation = ObjectAnimator.ofFloat(loading, "rotation", 0.0f, _splashTime);
            animation.setDuration(_splashTime * 2);
            animation.start();
            loading.setScaleX(1.0f);

            Thread splashTread = new Thread() {
                @Override
                public void run() {
                    try {
                        int waited = 0;
                        while(_active && (waited < _splashTime)) {
                            sleep(200);
                            if(_active) {
                                waited += 200;
                            }
                        }
                    } catch(InterruptedException e) {
                        //do nothing
                    } finally {
                        finish();
//Create new activity based on and intent with CurrentActivity
                        Intent intent = new Intent(Splashpage.this, Mainmenu.class);
                        startActivity(intent);
                    }
                }
            };
            splashTread.start();
            return true;
        }
    return false;
    }

}
