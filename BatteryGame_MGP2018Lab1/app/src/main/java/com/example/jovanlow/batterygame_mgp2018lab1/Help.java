package com.example.jovanlow.batterygame_mgp2018lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Help extends Activity implements OnClickListener {

    //Define an object and pass it to another method to use
    private Button btn_start, btn_menu;


    //Annotation to assure that the subclass method is overriding the parent class method. If it does not, compile with error
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //code for hiding UI
        requestWindowFeature(Window.FEATURE_NO_TITLE); //hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //hide top bar

        setContentView(R.layout.help);

        //Set listener to button
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);
        btn_menu = (Button) findViewById(R.id.btn_menu);
        btn_menu.setOnClickListener(this);
    }

    @Override // will happen if there is an tap of the button on the screen/view
    public void onClick(View v)
    {
        Intent intent = new Intent(); //Intent = action to be performed

        //Intent is an object that provides runtime binding (e.g 2 or more activities occurring at one time)

        if (v == btn_start)
        {
            intent.setClass(this, Gamepage.class);
        }
        else if (v == btn_menu)
        {
            intent.setClass(this, Mainmenu.class);
        }
        startActivity(intent);

    }

}
