package com.example.jovanlow.batterygame_mgp2018lab1;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

//Making this class a SurfaceView class
public class Gameview extends SurfaceView {

    private SurfaceHolder holder = null;

    private Updatethread updatethread = new Updatethread(this);

    public Gameview(Context _context)
    {
        super (_context);
        holder = getHolder();

        //game info?
        if(holder != null)
        {
            holder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    //Setup stuff to start Updatethread
                    if (!updatethread.IsRunning())
                    {
                        updatethread.Initialized();
                    }

                    if (!updatethread.isAlive())
                    {
                        updatethread.start();
                    }

                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                    //Do nothing because Updatethread will handle it

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    //Terminate the thread
                    updatethread.Terminate();
                }
            });
        }

    }

}
