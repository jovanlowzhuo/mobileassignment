package com.example.jovanlow.batterygame_mgp2018lab1;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.View.OnHoverListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.io.Console;


public class Mainmenu extends Activity implements OnClickListener, OnHoverListener, OnFocusChangeListener {

    //Define an object and pass it to another method to use
    private Button btn_start, btn_help, btn_equip, btn_newcity, btn_settings;
    private RelativeLayout layout;


    //Annotation to assure that the subclass method is overriding the parent class method. If it does not, compile with error
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //code for hiding UI
        requestWindowFeature(Window.FEATURE_NO_TITLE); //hide title
        setContentView(R.layout.mainmenu);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //hide top bar

        //Set listener to button
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);
        //btn_start.setOnHoverListener(this);
        btn_start.setOnFocusChangeListener(this);
        btn_help = (Button) findViewById(R.id.btn_help);
        btn_help.setOnClickListener(this);
        //btn_help.setOnHoverListener(this);
        btn_help.setOnFocusChangeListener(this);

        btn_equip = (Button) findViewById(R.id.btn_equip);
        btn_equip.setOnClickListener(this);
        btn_equip.setOnFocusChangeListener(this);
        btn_newcity = (Button) findViewById(R.id.btn_newcity);
        btn_newcity.setOnClickListener(this);
        btn_newcity.setOnFocusChangeListener(this);
        btn_settings = (Button) findViewById(R.id.btn_settings);
        btn_settings.setOnClickListener(this);
        btn_settings.setOnFocusChangeListener(this);

        layout = (RelativeLayout)findViewById(R.id.myRelativeLayout);
        layout.setOnFocusChangeListener(this);

    }

    @Override // will happen if there is an tap of the button on the screen/view
    public void onClick(View v)
    {
        Intent intent = new Intent(); //Intent = action to be performed

        //Intent is an object that provides runtime binding (e.g 2 or more activities occurring at one time)

        if (v == btn_start)
        {
            intent.setClass(this, Gamepage.class);
            startActivity(intent);
        }
        else if (v == btn_help)
        {
            intent.setClass(this, Help.class);
            startActivity(intent);
        }
        else if (v == btn_equip)
        {
            intent.setClass(this, Equip.class);
            startActivity(intent);
        }
        else if (v == btn_newcity)
        {
            intent.setClass(this, Newcity.class);
            startActivity(intent);
        }
        else if (v == btn_settings)
        {
            intent.setClass(this, Settingspage.class);
            startActivity(intent);
        }


    }

    @Override // will happen if there is an hover above the button on the screen/view
    public boolean onHover(View v, MotionEvent m)
    {


        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus)
    {
        if (v == btn_start)
        {
            if (hasFocus) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_start, "scaleX", 1.4f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_start, "scaleY", 1.4f);
                animation.setDuration(200);
                animation.start();
            }
            else
            {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_start, "scaleX", 1.0f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_start, "scaleY", 1.0f);
                animation.setDuration(200);
                animation.start();
            }
        }
        if (v == btn_help)
        {
            if (hasFocus) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_help, "scaleX", 1.4f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_help, "scaleY", 1.4f);
                animation.setDuration(200);
                animation.start();
            }
            else
            {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_help, "scaleX", 1.0f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_help, "scaleY", 1.0f);
                animation.setDuration(200);
                animation.start();
            }
        }
        if (v == btn_equip)
        {
            if (hasFocus) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_equip, "scaleX", 1.4f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_equip, "scaleY", 1.4f);
                animation.setDuration(200);
                animation.start();
            }
            else
            {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_equip, "scaleX", 1.0f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_equip, "scaleY", 1.0f);
                animation.setDuration(200);
                animation.start();
            }
        }
        if (v == btn_newcity)
        {
            if (hasFocus) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_newcity, "scaleX", 1.4f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_newcity, "scaleY", 1.4f);
                animation.setDuration(200);
                animation.start();
            }
            else
            {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_newcity, "scaleX", 1.0f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_newcity, "scaleY", 1.0f);
                animation.setDuration(200);
                animation.start();
            }
        }
        if (v == btn_settings)
        {
            if (hasFocus) {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_settings, "scaleX", 1.2f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_settings, "scaleY", 1.2f);
                animation.setDuration(200);
                animation.start();
            }
            else
            {
                ObjectAnimator animation = ObjectAnimator.ofFloat(btn_settings, "scaleX", 1.0f);
                animation.setDuration(200);
                animation.start();
                animation = ObjectAnimator.ofFloat(btn_settings, "scaleY", 1.0f);
                animation.setDuration(200);
                animation.start();
            }
        }
        if (v == layout)
        {
            if (hasFocus)
            {

            }
            else
            {

            }
        }
    }


}
