package com.example.jovanlow.batterygame_mgp2018lab1;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;

public class Updatethread extends Thread {

    static final long targetFPS = 60;

    private Gameview view = null;

    private SurfaceHolder holder = null;

    private boolean isRunning = false;

    public Updatethread(Gameview _view)
    {
        view = _view;
        holder = view.getHolder();
        Batterygame.Instance.Init(view);
    }

    //check if it is running
    public boolean IsRunning()
    {
        return isRunning;
    }

    //check if things are initialized
    public void Initialized()
    {
        isRunning = true;
    }

    //Check if things have ended
    public void Terminate()
    {
        isRunning = false;
    }

    @Override
    public void run()
    {
        //IMPORTANT

        //Initialize some variables
        long framePerSecond = 1000 / targetFPS;
        long startTime = 0; //To deal with Frame Rate Controller

        long prevTime = System.nanoTime(); //to deal with Delta time

        while (IsRunning())
        {
            startTime = System.currentTimeMillis();

            long currTime = System.nanoTime();
            float deltaTime = (float)((currTime - prevTime) - 10000000.0);

            prevTime = currTime; //End of delta time

            //Do some updates
            Batterygame.Instance.Update(deltaTime);

            //Renderthings, Check if Canvas is empty or not
            Canvas canvas = holder.lockCanvas();

            if (canvas != null)
            {
                synchronized (holder) //Canvas is locked in here
                {
                    //Render things

                    canvas.drawColor(Color.BLUE);

                    //Your sample game scene
                    Batterygame.Instance.Render(canvas);
                }
                //Done rendering, can unlock canvas
                holder.unlockCanvasAndPost(canvas);
            }


            //Post Update
            try{
                long sleepTime = framePerSecond - (System.currentTimeMillis() - startTime);

                if (sleepTime > 0)
                {
                    sleep(sleepTime);
                }

            }
            catch(InterruptedException e)
            {
                Terminate();
            }



        }


    }

}
