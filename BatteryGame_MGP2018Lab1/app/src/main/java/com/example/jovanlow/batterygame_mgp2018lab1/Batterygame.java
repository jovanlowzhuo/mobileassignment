package com.example.jovanlow.batterygame_mgp2018lab1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

//Your game will be here babyy
public class Batterygame {

    //Create a singleton
    public final static Batterygame Instance = new Batterygame();

    //Game stuff, etc.
    private Bitmap bmp; //for loading of images
    float offset = 0.0f;

    private Batterygame()
    {

    }

    //Init
    public void Init(SurfaceView _view)
    {
        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.button); //load images

    }

    //Update
    public void Update(float dt)
    {
        offset += dt;
    }

    //Render
    public void Render(Canvas _canvas)
    {
        int currOffset = (int)(offset * 100.0f);
        _canvas.drawBitmap(bmp, (10 + currOffset)% 500, 10, null);
    }


}
